import { Logger } from '../utils/Logger';
import {APOLLO_URI, APOLLO_TOKEN, APOLLO_AUTH_URI, TOKEN_USER_ID,SELECTED_ENVIRONMENT} from '../config/constants'
import { RetryLink } from "@apollo/link-retry";
import { onError } from "@apollo/link-error";
import { ApolloClient,gql, HttpLink, InMemoryCache, ApolloLink } from '@apollo/client';
import { Alert } from 'react-native';
import I18n from '../config/language';

const logger = new Logger('GraphQLClient');
const GET_TOKEN = gql`
  query getToken($userId: ID!) {
    getToken(userId:$userId){
    token
  }
}`;

let token :string|undefined;//= 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InNlcnZpY2UiOiJzdWRva3VjcnVzaEBkZXYiLCJyb2xlcyI6WyJhZG1pbiJdfSwiaWF0IjoxNTcyNDI5MDg3LCJleHAiOjE1NzMwMzM4ODd9.wmDIc8OZ5zk9qWU0i1Kg4fpL5b2xJufQzOrxl0teE1g';//undefined ;
const authHttpLink = new HttpLink({  uri: APOLLO_AUTH_URI}); //'APOLLO_AUTH_URI'

export const authClient = new ApolloClient({ // (token:string) =>
  link: new RetryLink().concat(authHttpLink),  
  cache: new InMemoryCache({
    dataIdFromObject: e => e.id
  })
});

const tokenLink = new ApolloLink((operation, forward) => {
  operation.setContext(({ headers }) => ({ headers: {
    authorization: token?`Bearer ${token}`:null, // however you get your token
    ...headers
  }}));
  return forward(operation);
});

export const retrieveToken = async() => {
  if(token || SELECTED_ENVIRONMENT!==2){
    return token;
  }
  try{
    logger.info('retrieveToken started token='+token);
    const queryResult = await authClient.query({query:GET_TOKEN,variables:{userId:TOKEN_USER_ID}})
    token = queryResult.data.getToken.token;
    logger.info('retrieveToken token='+token);
  }catch(err){
    logger.error('Connection Problem',err);
    Alert.alert(I18n.t('Connection Problem'))
  }
  
  return token;
  
}


const httpLink = new HttpLink({  
  uri: APOLLO_URI,
});
const errorLink = onError( ({ graphQLErrors, networkError, operation, forward }) => {
  if (graphQLErrors) {
    for (let err of graphQLErrors) {
      logger.error('errrr=',err);
      if (err.message.indexOf('invalid token')>=0 || err.message.indexOf('Your token is invalid')>=0 || err.message.indexOf('jwt must be provided')>=0) {
        
          // error code is set to UNAUTHENTICATED
          // when AuthenticationError thrown in resolver

          // modify the operation context with a new token
          authClient.query({query:GET_TOKEN,variables:{userId:TOKEN_USER_ID}}).then(res=>{
            logger.log('authClient res=',res);
            token = res.data.getToken.token;
            const oldHeaders = operation.getContext().headers;
          operation.setContext({
            headers: {
              ...oldHeaders,
              authorization: `Bearer ${token}`,
            },
          });
          }).catch(err=>logger.error('refetch token error',err))
          
          // retry the request, returning the new observable
          //return forward(operation);
      }
    }
  }
  if (networkError) {
    logger.log(`[Network error]: ${networkError}`);
    // if you would also like to retry automatically on
    // network errors, we recommend that you use
    // apollo-link-retry
  }
}
);

const link = new RetryLink().concat(
  // split based on operation type
  
  //authLink.concat(timeoutHttpLink.concat(httpLink)),
  tokenLink.concat(errorLink.concat(httpLink)),
);


export const apolloClient = new ApolloClient({ // (token:string) =>
link,
cache: new InMemoryCache({
  dataIdFromObject: e => e.id
}),
resolvers: {},
});
