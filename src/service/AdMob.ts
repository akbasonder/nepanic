import { Logger } from '../utils/Logger';
import {Platform} from 'react-native';
import { RewardedAd, RewardedAdEventType } from '@react-native-firebase/admob';

export const IS_IOS: boolean = Platform.OS === 'ios' || Platform.OS === 'macos'
const logger = new Logger('AdMob');

export const showAd = (navigation) => {
    logger.info('showAd started');
    const adId = IS_IOS?'ca-app-pub-9295520885514483/4950970081':'ca-app-pub-9295520885514483/2234620482';
    const rewarded = RewardedAd.createForAdRequest(adId);
    let isWatched = false;
    const eventListener = rewarded.onAdEvent((type, error, reward) => {
        logger.info(' RewardedAdEventType is',type);
        if (type === RewardedAdEventType.LOADED) {
            rewarded.show();
        }  
        if (type === RewardedAdEventType.EARNED_REWARD) {
          logger.info('User earned reward of ', reward);
          isWatched = true;
        }
        if (type === 'closed') {
            if(!isWatched){
                navigation.goBack();
            }
        }
    });
    rewarded.load();     
}