import I18n from 'i18n-js';

I18n.fallbacks = true;
//I18n.locale = 'tr';

I18n.translations = {
    'en': {   
        'Home':'Home',
        'Support':'Support',
        'Pro':'Pro',
        'Menu':'Menu',
        'My Diary':'Diary',
        'Connection Problem':'Connection Problem',
        'You have to approve User Aggrement':'You have to approve User Aggrement',
        'Are you sure to delete this record?':'Are you sure to delete this record?',
        'Warning':'Warning',
        'Cancel':'Cancel',
        'OK':'OK',
        'You have to enter title and content':'You have to enter title and content',
        'Compose Diary':'Compose Diary',
        'Edit Diary':'Edit Diary',
        'Save Diary':'Save Diary',
        'START':'START',
        'Minute':'Minute',
        'Seconds':'Seconds',
        'Write title here':'Write title here',
        'Write here what you feel today':'Write here what you feel today',
    },
    'tr':
    {
        'Home' : 'Giriş',        
        'Support':'Destek',
        'Pro':'Pro',
        'Menu':'Menu',
        'My Diary':'Günlük',
        'Connection Problem':'Bağlantı Sorunu',
        'You have to approve User Aggrement':'Kullanıcı Sözleşmesini onaylamanız gerekmektedir',
        'Are you sure to delete this record?': 'Bu kaydı silmek istediğinizden emin misiniz?',
        'Warning':'Uyarı',
        'Cancel':'İptal',
        'OK':'Tamam',
        'You have to enter title and content':'Başlık ve içerik girmek zorunludur',
        'Compose Diary':'Günlük oluştur',
        'Edit Diary':'Günlüğü düzenle',
        'Save Diary':'Günlüğü kaydet',
        'START':'BAŞLA',
        'Minute':'Dakika',
        'Seconds':'Saniye',
        'Write title here':'Başlığı buraya ekleyin',
        'Write here what you feel today':'Bugün hissettiklerini buraya yaz',
    }
}

export default I18n;
