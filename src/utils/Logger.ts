//import moment from 'moment';
import { LOG_LEVEL, LogLevel } from '../config/constants'

export class Logger {

    private name: string;

    constructor(name: string) {
        this.name = name;
    }

    trace(message?: any, ...optionalParams: any[]) {
        if (LOG_LEVEL >= LogLevel.TRACE) {
            //console.trace('[TRACE] '.concat(moment().format('HH:mm:ss.SSS')).concat(' ' + this.name + ' - ').concat(message), ...optionalParams);
        }
    }

    debug(message?: any, ...optionalParams: any[]) {
        if (LOG_LEVEL >= LogLevel.DEBUG) {
            console.debug(message, ...optionalParams);  //,...optionalParams
        }
    }

    log(message?: any, ...optionalParams: any[]) {
        if (LOG_LEVEL >= LogLevel.LOG) {
            console.log(message, ...optionalParams); //,...optionalParams
        }
    }

    info(message?: any, ...optionalParams: any[]) {
        if (LOG_LEVEL >= LogLevel.INFO) {
            console.log(message, ...optionalParams);   //,...optionalParams            
        }
    }

    warn(message?: any, ...optionalParams: any[]) {
        if (LOG_LEVEL >= LogLevel.WARN) {
            console.log(message, ...optionalParams); //...optionalParams
        }
    }

    error(message?: any, ...optionalParams: any[]) {
        if (LOG_LEVEL >= LogLevel.ERROR) {
            console.log(message, ...optionalParams);
        }
    }
}
