import React, { useState, useContext } from 'react';
import {
    View,
    Text,
    Dimensions,
    StyleSheet,
    TouchableOpacity,
    TouchableHighlight,
    TextInput,
    Alert,    
    KeyboardAvoidingView,
} from 'react-native';
import { SUPPORTED_LOCALES, fontSizeMultiplier, COLUMN_COUNT,  TOP_HEADER_HEIGHT,  } from '../config/constants';
import I18n from '../config/language';
import { gql, useQuery, useMutation } from '@apollo/client';
import { showError, showLoading } from './Main'
import { Logger } from '../utils/Logger';
import { createStackNavigator } from '@react-navigation/stack';
import { SwipeListView } from 'react-native-swipe-list-view';
import { SwipeRow } from 'react-native-swipe-list-view';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faPlus, faTrashAlt, faEdit, faExclamationTriangle, faBackward, faArrowLeft } from '@fortawesome/free-solid-svg-icons'
import { apolloClient } from '../service/GraphQLClient';
import EmptyDiaryTR from '../../assets/warnings/Uyari-Gunluk-Bos.svg';
import EmptyDiaryEN from '../../assets/warnings/Uyari-Diary-Bos.svg';
import ButtonAddDiaryTR from '../../assets/buttons/Button-GunlukOlustur.svg';
import ButtonAddDiaryEN from '../../assets/buttons/Button-DiaryOlustur.svg';
import ButtonSaveDiaryTR from '../../assets/buttons/Button-Gunluk-Kaydet.svg';
import ButtonSaveDiaryEN from '../../assets/buttons/Button-Diary-Kaydet.svg';
import ButtonEditDiaryTR from '../../assets/buttons/Button-Gunluk-Duzenle.svg';
import ButtonEditDiaryEN from '../../assets/buttons/Button-Diary-Edit.svg';
import TitleGunlugumTR from '../../assets/titles/Title-Tr-Gunlugum.svg';
import TitleGunlugumEN from '../../assets/titles/Title-En-Gunlugum.svg';
import BackButton from '../../assets/buttons/Button-Back-Geri.svg';
import moment from 'moment';
import { AppContext } from '../../App';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const logger = new Logger('Diary');

const GET_DIARIES = gql`
  query diaries($userId:ID) {      
    diaries(
      orderBy:createdAt_DESC
      where:{
        status:"active"
        user:{
            id:$userId
        }
    } ){
      id
      createdAt      
    }
  }
`;

const GET_DIARY = gql`
  query diary($id:ID) {
    diary(
      where:{
        id:$id         
    } ){
      id
      title
      content
    }
  }
`;

const CREATE_DIARY = gql`
  mutation createDiary($title: String!,$content:String!,$userId:ID!) {
    createDiary(
        data:{
            title: $title,
            content:$content,
            user:{connect:{id:$userId}}
        }) 
    {
      id      
      title
      createdAt
    }
  }
`;

const UPDATE_DIARY = gql`
  mutation updateDiary($title: String,$content:String,$id:ID!,$status:String) {
    updateDiary(
        where:{
            id:$id
        }
        data:{
            title: $title,
            content:$content,       
            status:$status     
        }) 
    {
      id      
      title
      createdAt
    }
  }
`;

const DiaryStack = createStackNavigator();

export const DiaryScreen = ({ navigation }) => {
    return (
        <DiaryStack.Navigator >
            <DiaryStack.Screen
                name="ListDiariesNav"
                component={ListDiaryScreen}
                options={{
                    headerBackTitle: 'Back',
                }}
            />
            <DiaryStack.Screen
                name="AddDiaryNav"
                component={AddDiaryScreen}
                options={{
                    headerBackTitle: 'Back'
                }}
            />
            <DiaryStack.Screen
                name="EditDiaryNav"
                component={EditDiaryScreen}
                options={{
                    headerBackTitle: 'Back'
                }}
            />
            <DiaryStack.Screen
                name="ViewDiaryNav"
                component={ViewDiaryScreen}
                options={{
                    headerBackTitle: 'Back'
                }}
            />
        </DiaryStack.Navigator>
    )
}

const TopHeader = ({withBackButton=true,lang='tr',navigation})=>{
    return <View style={{marginLeft:10,marginRight:5}}>
    <View style={{ borderBottomColor:'rgb(224,224,224)',borderBottomWidth:1,borderTopColor:'rgb(224,224,224)',borderTopWidth:1, height:70,alignItems:'center',justifyContent:withBackButton?'space-between':'flex-start', flexDirection:'row',marginLeft:10,marginRight:10}}>
            {lang==='tr' && <TitleGunlugumTR width={150} height={40}/>}
            {lang==='en' && <TitleGunlugumEN width={150} height={40}/>}
            {withBackButton && <TouchableOpacity onPress={()=>{navigation.goBack()}}>
                    <BackButton width={40} height={40}/>
                </TouchableOpacity>
            }
        </View>
    </View>

}

const ListDiaryScreen = ({ navigation }) => {
    const context = useContext(AppContext);
    const lang = context.lang.get;
    const userId = context.userId.get;
    const { loading, error, data } = useQuery(GET_DIARIES, { variables: { userId: userId } });
    if (loading) return showLoading(lang);
    if (error) return showError();
    //backgroundColor: 'dodgerblue', width: SCREEN_WIDTH * 0.6, height: 40, borderRadius: 20, flexDirection: 'row', borderWidth: 1, borderColor: 'rgb(128,128,128)', alignItems: 'center', justifyContent: 'center',
    //<FontAwesomeIcon icon={faPlus} size={30} style={{}} color={'white'} />
    //<Text style={{ marginLeft: 10, color: 'white', fontWeight: '700', fontSize: 20 * fontSizeMultiplier }}> {I18n.t('Compose Diary')}</Text>
    return (
        <View style={styles.container}>
            <TopHeader withBackButton={false} lang={lang} navigation={navigation}/>
            <View style={{ width: SCREEN_WIDTH, alignItems: 'center' }}>
                <TouchableOpacity style={{ marginTop:10 }}  onPress={() => { navigation.navigate('AddDiaryNav') }}>
                    {lang==='tr' &&<ButtonAddDiaryTR width={200} height={50}/>}
                    {lang==='en' &&<ButtonAddDiaryEN width={200} height={50}/>}
                </TouchableOpacity>
            </View>
            {data.diaries.length ===0?<View style={{marginTop:20,width:SCREEN_WIDTH,alignItems:'center'}}>
                   {lang==='tr' && <EmptyDiaryTR width={SCREEN_WIDTH} height={SCREEN_WIDTH}></EmptyDiaryTR>}
                   {lang==='en' && <EmptyDiaryEN width={SCREEN_WIDTH} height={SCREEN_WIDTH}></EmptyDiaryEN>}

            </View>

            
            :data.diaries.map((diary, index) => <View key={diary.id}>
                <View style={styles.spacer} />
                <SwipeRow disableRightSwipe rightOpenValue={-120} >
                    <View style={styles.standaloneRowBack}>
                        <TouchableOpacity style={{ backgroundColor: 'blue', height: 70, width: 60, justifyContent: 'center', alignItems: 'center' }}
                            onPress={async () => {
                                try {
                                    const queryResult = await apolloClient.query({ query: GET_DIARY, variables: { id: diary.id } })
                                    logger.log('queryResult is', queryResult);
                                    navigation.navigate('EditDiaryNav', { diaryId: diary.id, title: queryResult.data.diary.title, content: queryResult.data.diary.content });
                                } catch (err) {
                                    logger.error('GET_DIARY error', err);
                                    Alert.alert(I18n.t('Connection problem'));
                                }
                            }}>
                            <FontAwesomeIcon icon={faEdit} size={30} style={{}} color={'white'} />
                        </TouchableOpacity>
                        <TouchableOpacity style={{ backgroundColor: 'purple', height: 70, width: 60, justifyContent: 'center', alignItems: 'center' }}
                            onPress={() => {
                                Alert.alert(I18n.t('Warning'), (I18n.t('Are you sure to delete this record?')), [
                                    {
                                        text: I18n.t('Cancel'), style: 'cancel', onPress: () => {
                                        }
                                    },
                                    {
                                        text: I18n.t('OK'), onPress: () => {                                            
                                            apolloClient.mutate({
                                                mutation: UPDATE_DIARY, variables: { id: diary.id, status: "deleted" }
                                                , refetchQueries: [
                                                    { query: GET_DIARIES, variables: { userId: userId } },{query:GET_DIARY,variables:{id:diary.id}}]
                                            })
                                                .then(() => { logger.log('Diary was deleted'); })
                                                .catch(err => {
                                                    logger.error('Delete diary error', err);
                                                    Alert.alert(I18n.t('Connection Problem'));
                                                });
                                        }
                                    },
                                ])
                            }}>
                            <FontAwesomeIcon icon={faTrashAlt} size={30} style={{}} color={'white'} />
                        </TouchableOpacity>
                    </View>
                    <TouchableHighlight onPress={() => {
                        navigation.navigate('ViewDiaryNav',{diaryId:diary.id});
                    }}>
                        <View style={{ backgroundColor: 'white', borderColor: 'rgb(192,192,192)', borderWidth: 1, height: 70, flexDirection: 'row',marginLeft:10,marginRight:10 }}>
                            <View style={{ marginLeft: 20, flex: 0.6, flexDirection: 'column', justifyContent: 'center' }}>
                                <Text style={{fontSize:20*fontSizeMultiplier,fontFamily:'CoreSansD45Medium',color:'rgb(128,128,128)'}}>{moment(diary.createdAt).format('LL')}</Text>
                                <Text style={{fontSize:20*fontSizeMultiplier,fontFamily:'CoreSansD45Medium',color:'rgb(128,128,128)'}}>{moment(diary.createdAt).format('dddd')}</Text>
                            </View>
                            <View style={{ flex: 0.35, alignItems: 'flex-end', justifyContent: 'center' }}>
                                <Text></Text>
                                <Text style={{fontSize:20*fontSizeMultiplier,fontFamily:'CoreSansD45Medium',color:'rgb(128,128,128)'}}>{moment(diary.createdAt).format('HH:mm')}</Text>
                            </View>
                        </View>
                    </TouchableHighlight>
                </SwipeRow>

            </View>
            )}
            <View style={{height:100}}/>
        </View>
    );
}

const AddDiaryScreen = ({ navigation }) => {
    const [title, setTitle] = useState('');
    const [content, setContent] = useState('');
    const context = useContext(AppContext);
    const lang = context.lang.get;
    const userId = context.userId.get;
    logger.info('AddDiary userId is',userId);
    
    const [createDiary, { loading, error, data }] = useMutation(
        CREATE_DIARY,
      /*  {
            update(cache, { data: { createDiary } }) {
                const { diaries } = cache.readQuery({ query: GET_DIARIES, variables: { userId: userId } });
                cache.writeQuery({
                    query: GET_DIARIES,
                    variables: { userId: userId },
                    //data: { diaries: diaries.concat([createDiary]) },
                    data: { diaries: [createDiary].concat([diaries]) },
                });
            }
        }*/
    );

    return (
        <View style={styles.container}>
            <TopHeader withBackButton={true} lang={lang} navigation={navigation}/>
            <View style={{  borderWidth: 1, height: 50, marginTop: 10, borderColor: 'rgb(128,128,128)', marginLeft: 20,marginRight:15, justifyContent: 'center' }}>
                <TextInput style={{ marginLeft: 15 }} placeholder={I18n.t('Write title here')}
                    value={title} onChangeText={(text) => { logger.log('header Text is', text); setTitle(text); }}
                />
            </View>
            
            <View  style={{marginLeft:20,marginRight:15,height:150, borderWidth: 1, marginTop: 10, borderColor: 'rgb(128,128,128)', }}>
                <TextInput style={{height:150, marginLeft: 15,}} multiline={true} numberOfLines={20} placeholder={I18n.t('Write here what you feel today')}
                    value={content} onChangeText={(text) => { logger.log('content Text is', text); setContent(text); }}
                />
            </View>
                
                <KeyboardAvoidingView enabled style={{ marginLeft:15,marginRight:15, alignItems:'center', marginTop: 10, }}>
                 <TouchableOpacity 
                     onPress={() => {
                        if (!title || !content) {
                            Alert.alert(I18n.t('You have to enter title and content'));
                            return;
                        }
                        logger.info('add diary title='+title+' content='+content+' userId='+userId);
                        createDiary({ variables: { title, content, userId: userId } ,
                            refetchQueries: [
                                { query: GET_DIARIES, variables: { userId: userId } }]
                        });
                        navigation.navigate('ListDiariesNav');
                    }}>
                 
                    {lang==='tr' && <ButtonSaveDiaryTR width={200} height={50}/>}
                    {lang==='en' && <ButtonSaveDiaryEN width={200} height={50}/>}
                 </TouchableOpacity>       
                </KeyboardAvoidingView>
                
           
            
                        
           
            {error && showError()}

        </View>
    );
}


const EditDiaryScreen = ({ route, navigation }) => {

    const diaryId = (route && route.params && route.params.diaryId) ? route.params.diaryId : undefined;
    if (!diaryId) return showError();
    const initialTitle = (route && route.params && route.params.title) ? route.params.title : undefined;
    const initialContent = (route && route.params && route.params.content) ? route.params.content : undefined;
    const context = useContext(AppContext);
    const lang = context.lang.get;
    const userId = context.userId.get;
    const [title, setTitle] = useState(initialTitle);
    const [content, setContent] = useState(initialContent);
    //const [updateDiary, { loading: mutationLoading, error: mutationError, data: mutationData }] = useMutation(UPDATE_DIARY);

    return (
        <View style={styles.container}>
            <TopHeader withBackButton={true} lang={lang} navigation={navigation}/>
            <View style={{  borderWidth: 1, height: 50, marginTop: 10, borderColor: 'rgb(128,128,128)', marginLeft: 20,marginRight:15, justifyContent: 'center' }}>
                <TextInput style={{ marginLeft: 15 }} placeholder={I18n.t('Write title here')}
                    value={title} onChangeText={(text) => { logger.log('header Text is', text); setTitle(text); }}
                />
            </View>
            
            <View  style={{marginLeft:20,marginRight:15,height:150, borderWidth: 1, marginTop: 10, borderColor: 'rgb(128,128,128)', }}>
                <TextInput style={{height:150, marginLeft: 15,}} multiline={true} numberOfLines={20} placeholder={I18n.t('Write here what you feel today')}
                    value={content} onChangeText={(text) => { logger.log('content Text is', text); setContent(text); }}
                />
            </View>

            <KeyboardAvoidingView enabled style={{ marginLeft:15,marginRight:15, alignItems:'center', marginTop: 10, }}>
                <TouchableOpacity 
                    onPress={() => {
                        if (!title || !content) {
                            Alert.alert(I18n.t('You have to enter title and content'));
                            return;
                        }
                       // updateDiary({ variables: { id: diaryId, title, content, userId } });
                       apolloClient.mutate({
                        mutation: UPDATE_DIARY, variables: { id: diaryId, title, content, userId }
                        , refetchQueries: [
                            { query: GET_DIARIES, variables: { userId: userId } },{query:GET_DIARY,variables:{id:diaryId}}]
                    })
                        .then(() => { logger.log('Diary was deleted'); })
                        .catch(err => {
                            logger.error('Delete diary error', err);
                            Alert.alert(I18n.t('Connection Problem'));
                        });
                        navigation.navigate('ListDiariesNav');
                    }}>
                    {lang==='tr' && <ButtonEditDiaryTR width={200} height={50}/>}
                    {lang==='en' && <ButtonEditDiaryEN width={200} height={50}/>}
                </TouchableOpacity>
            </KeyboardAvoidingView>
           

        </View>
    );
}

const ViewDiaryScreen = ({ route, navigation }) => {
    const context = useContext(AppContext);
    const lang = context.lang.get;

    const diaryId = (route && route.params && route.params.diaryId) ? route.params.diaryId : undefined;
    if (!diaryId) return showError();
    const { loading, error, data } = useQuery(GET_DIARY, { variables: { id: diaryId } });
    if (loading) return showLoading(lang);
    if (error) return showError();

    

    return (
        <View style={styles.container}>
            <TopHeader withBackButton={true} lang={lang} navigation={navigation}/>
            <View style={{ marginLeft: 30, marginTop: 10,marginRight:20 }}>
                <Text style={{ fontSize: 32 * fontSizeMultiplier, fontWeight: '600',color:'rgb(128,128,128)' }}>{data.diary.title}</Text>
                <Text style={{ fontSize: 16 * fontSizeMultiplier,marginTop:20, color:'rgb(128,128,128)' }}>{data.diary.content}</Text>                
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,        
    },
    standalone: {
        marginTop: 30,
        marginBottom: 30,        
    },
    standaloneRowFront: {
        alignItems: 'center',
        backgroundColor: '#CCC',
        justifyContent: 'center',        
    },
    standaloneRowBack: {
        alignItems: 'center',
        //backgroundColor: '#8BC645',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',        
        marginRight:10
        //padding: 15,
        //borderWidth:2,
        //borderColor:'white'

    },
    backTextWhite: {
        color: '#FFF',
    },
    spacer: {
        height: 10,
    },
});