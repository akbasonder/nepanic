import React, { useState, useEffect, useContext } from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Image,
  Platform,
  Alert,
} from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faLiraSign } from '@fortawesome/free-solid-svg-icons'
import { fontSizeMultiplier, BOTTOM_TAB_HEIGHT, TOP_HEADER_HEIGHT,  USER_ID, UserTypeEnum, PaymentTypeEnum, USER_TYPE, SELECTED_ENVIRONMENT } from '../config/constants';
import RNIap, {
  purchaseErrorListener,
  purchaseUpdatedListener,
  ProductPurchase,
  PurchaseError, InAppPurchase, SubscriptionPurchase,
  acknowledgePurchaseAndroid,

} from 'react-native-iap';
import { Logger } from '../utils/Logger';
import { gql, useMutation } from '@apollo/client';
import ProIcon from '../../assets/subicons/Icons-08.svg';
import WarningProTR from '../../assets/warnings/Uyari-Pro.svg';
import WarningProEN from '../../assets/warnings/Uyari-Pro-En.svg';
import WarningHataTR from '../../assets/warnings/Uyari-Hata.svg';
import WarningHataEN from '../../assets/warnings/Uyari-Hata-En.svg';
import InfoProTR1 from '../../assets/info/Pro-Yazi-1-TR.svg';
import InfoProTR2 from '../../assets/info/Pro-Yazi-2-TR.svg';
import InfoProTR3 from '../../assets/info/Pro-Yazi-3-TR.svg';
import InfoProEN1 from '../../assets/info/Pro-Yazi-1-EN.svg';
import InfoProEN2 from '../../assets/info/Pro-Yazi-2-EN.svg';
import InfoProEN3 from '../../assets/info/Pro-Yazi-3-EN.svg';
import ButtonAbonelikTR from '../../assets/buttons/Button-AbonelikGeriYukle.svg';
import ButtonAbonelikEN from '../../assets/buttons/Button-RestoreAppPurchase.svg';
import TitlePro from '../../assets/titles/Title-Tr-Pro.svg';
import { apolloClient } from '../service/GraphQLClient';
import AsyncStorage from '@react-native-community/async-storage';
import { AppContext } from '../../App';
import I18n from '../config/language';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const logger = new Logger('Pro');

const itemSkus = Platform.select({
  ios: [
    'iosremoveads',
    //'removeadsbigger',

  ],
  android: [
    SELECTED_ENVIRONMENT===2?'removeadsandroid':'android.test.purchased',    
  ],
});

const UPDATE_USER = gql`
  mutation updateUser($id:ID!,$user:UserUpdateInput!) {
  updateUser(
    where:{id:$id}
    data:$user
  )
  {
    id
    createdAt    
  }
}`;


const GET_USER = gql`
  query user($id:ID) {
    user(
      where:{
        id:$id         
    } ){
      userType
    }
  }
`;

let productList;

let purchaseUpdateSubscription;
let purchaseErrorSubscription;



//TODO: remove Ads yapilacak. Reklam gormemeli.
export const ProScreen = ({ navigation }) => {
  const [currencyCode,setCurrencyCode] = useState('USD');
  const [price,setPrice] = useState(29.99);
  const context = useContext(AppContext);  
  const lang = context.lang.get;
  logger.info('lang==='+lang);
  const userType= context.userType.get;
  const setUserType = context.userType.set; 
  useEffect(() => {
    initIAP(setUserType,setCurrencyCode,setPrice);
  }),[setUserType,setCurrencyCode,setPrice];
/*<View style={{ height: 60, width: SCREEN_WIDTH, alignItems: 'center', borderBottomColor: 'rgb(128,128,128)', borderBottomWidth: 1, flexDirection: 'row' }}>
      <ProIcon style={{ marginLeft: 15, }} width={SCREEN_WIDTH / 10} height={SCREEN_WIDTH / 10} />
      <Text style={{ color: 'blue', marginLeft: 10, fontSize: 28 * fontSizeMultiplier, fontWeight: '700' }}>Pro</Text>
    </View>
    */
  return <View style={{ backgroundColor: 'white', height: SCREEN_HEIGHT - BOTTOM_TAB_HEIGHT - TOP_HEADER_HEIGHT }}>
      <TopHeader/>
      <View style={{ width: SCREEN_WIDTH - 40, marginLeft: 20, alignItems: 'center' }}>      
        {lang==='tr' && getTurkishContent(userType,setUserType,price,currencyCode)}
        {lang==='en' && getEnglishContent(userType,setUserType,price,currencyCode)}

    </View>

  </View>

}

/*const TopHeader = ()=>{
  return <View style={{marginLeft:20,marginRight:15, height:70,alignItems:'center',flexDirection:'row',  borderBottomColor:'rgb(224,224,224)',borderBottomWidth:1,borderTopColor:'rgb(224,224,224)',borderTopWidth:1}}>          
          <ProIcon width={50} height={50} />
          <Text style={{ color: 'blue', marginLeft: 10, fontSize: 36 * fontSizeMultiplier, fontWeight: '600' }}>Pro</Text>
  </View>
}*/
const TopHeader = ()=>{
  return <View style={{marginLeft:20,marginRight:15, height:70,justifyContent:'center', borderBottomColor:'rgb(224,224,224)',borderBottomWidth:1,borderTopColor:'rgb(224,224,224)',borderTopWidth:1}}>
          <TitlePro width={120} height={40} />
          
  </View>
}

const getTurkishContent = (userType:number,setUserType,price:number,currencyCode:string) =>{
  logger.info('userType is ',userType);
  return (userType===UserTypeEnum.FREE?
  <View style={{alignItems:'center'}}>
      <InfoProTR1 width={SCREEN_WIDTH-40} height={180} style={{marginTop:30}}/>
      
      <TouchableOpacity style={{ flexDirection:'row', backgroundColor: 'blue', justifyContent: 'center', alignItems: 'center',marginTop:10, height:70,width:SCREEN_WIDTH/2 }}
        onPress={async()=>{
          const products = await RNIap.getProducts(itemSkus);
          requestPurchase(products[0].productId);
        }}>
            <Text style={{ color: 'white', fontSize: 28 * fontSizeMultiplier, fontWeight: '700' }}>{price>0?price:''}</Text>
            <Text style={{ color: 'white', fontSize: 28 * fontSizeMultiplier, fontWeight: '700',marginLeft:10 }}>{currencyCode}</Text>      
      </TouchableOpacity>

      <InfoProTR2 width={SCREEN_WIDTH*0.6} height={30} style={{marginTop:10}}/>
      <InfoProTR3 width={SCREEN_WIDTH-40} height={80} style={{marginTop:10}}/>

      <TouchableOpacity style={{marginTop:10 }}
        onPress={()=>getAvailablePurchases(setUserType)}>
         <ButtonAbonelikTR width={SCREEN_WIDTH*0.8} height={SCREEN_WIDTH*0.2} />
      </TouchableOpacity>    

  </View>:
  <WarningProTR width={SCREEN_WIDTH} height={SCREEN_WIDTH*1.6} style={{marginTop:SCREEN_WIDTH*-0.1}}></WarningProTR>  
  )
  
}

const getEnglishContent = (userType:number,setUserType,price:number,currencyCode:string) =>{
  return (userType===UserTypeEnum.FREE?
    <View style={{alignItems:'center'}}>
    <InfoProEN1 width={SCREEN_WIDTH-40} height={180} style={{marginTop:30}}/>
      
      <TouchableOpacity style={{ flexDirection:'row', backgroundColor: 'blue', justifyContent: 'center', alignItems: 'center',marginTop:10, height:70,width:SCREEN_WIDTH/2 }}
        onPress={async()=>{
          const products = await RNIap.getProducts(itemSkus);
          requestPurchase(products[0].productId);
        }}>
            <Text style={{ color: 'white', fontSize: 28 * fontSizeMultiplier, fontWeight: '700' }}>{price>0?price:''}</Text>
            <Text style={{ color: 'white', fontSize: 28 * fontSizeMultiplier, fontWeight: '700',marginLeft:10 }}>{currencyCode}</Text>      
      </TouchableOpacity>
      
      <InfoProEN2 width={SCREEN_WIDTH*0.6} height={30} style={{marginTop:10}}/>
      <InfoProEN3 width={SCREEN_WIDTH-40} height={80} style={{marginTop:10}}/>
     
      <TouchableOpacity style={{ marginTop:10}}
        onPress={()=>getAvailablePurchases(setUserType)}>
         <ButtonAbonelikEN width={SCREEN_WIDTH*0.8} height={50} />
      </TouchableOpacity>   
  </View>
  :<WarningProEN width={SCREEN_WIDTH} height={SCREEN_WIDTH*1.6} style={{marginTop:SCREEN_WIDTH*-0.1}}/>
  )
}

const initIAP = async (setUserType,setCurrencyCode,setPrice) => {
  logger.info('initIAP started')
  await RNIap.initConnection();
  const products = await RNIap.getProducts(itemSkus);
  logger.info('Products2222222', products);
  const price = products[0].price;
  logger.info('price is',price);
  const paymentAmount = price.startsWith("\"")?price.substring(1,price.length-1):price;
  logger.info('paymentAmount is',paymentAmount);
  setCurrencyCode(products[0].currency);
  setPrice(parseFloat(paymentAmount));

  purchaseUpdateSubscription = purchaseUpdatedListener(
    async (purchase: ProductPurchase) => {
      
      logger.info('purchaseUpdatedListener', purchase);
      if (
        purchase.purchaseStateAndroid === 1 &&
        !purchase.isAcknowledgedAndroid
      ) {
        try {
          const ackResult = await acknowledgePurchaseAndroid(purchase.purchaseToken,);
          logger.log('ackResult', ackResult);
        } catch (ackErr) {
          logger.warn('ackErr', ackErr);
        }
      }
      //logger.log('provide sync with your DB for user Status'); 
      const userId = await AsyncStorage.getItem(USER_ID);
      logger.info('purchase result=', purchase);
      
      try {
        const user = {
          //id: userId,
          paymentType: PaymentTypeEnum.YEARLY,
          userType: UserTypeEnum.PAID,
          membershipStartDate: new Date(),
          paymentList: {
            create: {
              productId: purchase.productId,
              transactionDate: purchase.transactionDate.toString(),
              transactionId: purchase.transactionId,
              transactionReceipt: purchase.transactionReceipt,
              currencyCode: products[0].currency,
              paymentType: PaymentTypeEnum.MONTHLY,
              paymentAmount: parseFloat(paymentAmount)
            }
          }
        }
        
        await apolloClient.mutate({mutation:UPDATE_USER,variables:{id:userId,user}});
        AsyncStorage.setItem(USER_TYPE,UserTypeEnum.PAID.toString());
        logger.info('userType will be set after purchase');
        setUserType(UserTypeEnum.PAID);        
        
      } catch (err) {
        logger.error('payment update error', err);
      }

    })

  purchaseErrorSubscription = purchaseErrorListener(
    (error: PurchaseError) => {
      logger.error('purchaseErrorListener', error);      
    },
  )
}


//TODO: expiration var mi diye bak. Restore purchase icin kullaniliyor.
const getAvailablePurchases = async (setUserType) => {
  logger.info(
    'Get available purchases (non-consumable or unconsumed consumable)',
  );
  try {
    const purchases = await RNIap.getAvailablePurchases();
    if (purchases && purchases.length > 0) {
      const userId = await AsyncStorage.getItem(USER_ID);
      logger.info('Available purchases lenth:: ', purchases.length);
      const user = { userType: UserTypeEnum.PAID, paymentType: PaymentTypeEnum.YEARLY, restoreTransactionId: purchases[0].transactionId };
      await apolloClient.mutate({
        mutation: UPDATE_USER,
        variables: {id: userId,user}
      })
      AsyncStorage.setItem(USER_TYPE,UserTypeEnum.PAID.toString());
      logger.info('userType will be set after restore');
      setUserType(UserTypeEnum.PAID);
      
    }
  } catch (err) {
    logger.error('restore purchase error', err);
    Alert.alert(I18n.t('Connection problem'));
  }
};

const requestPurchase = async (sku) => {
  try {
    logger.debug('requestPurchase started')
    await RNIap.requestPurchase(sku);    
    logger.info('requestPurchase finished')
  } catch (err) {
    Alert.alert(I18n.t('Connection Problem'));    
    logger.error('payment error', err);
  }
};

