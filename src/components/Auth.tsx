import React, { useState, useEffect, useContext } from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Alert,
  ImageBackground,
  Image,
  ScrollView
} from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { SUPPORTED_LOCALES, fontSizeMultiplier, USER_ID, LANG, USER_TYPE, TOP_HEADER_HEIGHT, BOTTOM_TAB_HEIGHT, UserTypeEnum } from '../config/constants';
import I18n from '../config/language';
import AsyncStorage from '@react-native-community/async-storage';
import CheckBox from '@react-native-community/checkbox';
import RNUserIdentity, { ICLOUD_ACCESS_ERROR } from 'react-native-user-identity';
import { Logger } from '../utils/Logger';
import { gql, useQuery,useMutation } from '@apollo/client';
import { apolloClient, retrieveToken } from '../service/GraphQLClient';
import {AppContext,} from '../../App';
import messaging from '@react-native-firebase/messaging';
import admob, { MaxAdContentRating } from '@react-native-firebase/admob';

import {FetchAndShowImage} from '../components/Home'
import BackButton from '../../assets/buttons/Button-Back-Geri.svg';
import CopyRight from '../../assets/warnings/v2-Copyright.svg'
import WarningTurkish from '../../assets/warnings/Uyari-Turkce.svg'
import WarningEnglish from '../../assets/warnings/Uyari-English.svg'
import ButtonEnglish from '../../assets/buttons/Button-English.svg'
import ButtonTurkce from '../../assets/buttons/Button-Turkce.svg'
import ButtonOK from '../../assets/buttons/Button-Ok.svg'
import ButtonTamam from '../../assets/buttons/Button-Tamam.svg'
import Logo from '../../assets/headers/Logo.svg';
import moment from 'moment';
import 'moment/locale/tr';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

const logger = new Logger('Auth');
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

const AuthStack = createStackNavigator();


const GET_USER = gql`
  query user($accountId:String!) {
    user(
      where:{
        accountId:$accountId
    } ){
      id
      userType      
    }
  }
`;

const CREATE_USER = gql`
  mutation createUser($accountId:String) {
    createUser(
      data:{
        accountId:$accountId
    } ){
      id      
    }
  }
`;


export const AuthScreen = ({ navigation }) => {
  return (
    <AuthStack.Navigator headerMode="none" initialRouteName='FirstNav'>
      <AuthStack.Screen
        name="FirstNav"
        component={FirstScreen}
      />
      <AuthStack.Screen
        name="ChangeLanguageNav"
        component={ChangeLanguageScreen}
      />      
      <AuthStack.Screen
        name="UserAgreementNav"
        component={UserAgreementScreen}
      />
      <AuthStack.Screen
        name="TermsOfUseNav"
        component={TermsOfUseScreen}
      />
    </AuthStack.Navigator>
  )
}

const fetchUserIdentity = async ()=> {
	try {
    const result = await RNUserIdentity.getUserId()
    logger.info('fetchUserIdentity result',result)
		if (result === null) {      
      return undefined;
    } 
    return result;
	} catch(error) {
    logger.error('fetchUserIdentity error',error)
		if (error === ICLOUD_ACCESS_ERROR) {
			//alert('Please set up an iCloud account in settings')
		}
  }
  return undefined;
}

const FirstScreen = ({ navigation }) => {
  
  const context = useContext(AppContext);
  const setLang = context.lang.set;
  const setUserId =  context.userId.set;
  const setUserType =  context.userType.set;
  useEffect(async () => {
    try{
      logger.info('First sreen started');
      initNotification();
      initAdmob();
      const lang = await AsyncStorage.getItem(LANG);
      if(lang){
        setLang(lang);
        I18n.locale = lang;
      }
      //TODO: pay attention here
      //await AsyncStorage.removeItem(USER_ID);
      let userId = await AsyncStorage.getItem(USER_ID);
      logger.info('userId is',userId);
      await retrieveToken();
      if(!userId){
        let hasExist = false;
        let accountId = undefined;
        try{
          accountId = await fetchUserIdentity();
          logger.info('accountId',accountId);
          if(accountId){
            const queryResult = await apolloClient.query({query:GET_USER,variables:{accountId}});
            logger.info('queryResult',queryResult);
            if(queryResult.data.user){
              userId = queryResult.data.user.id;
              const userType :number |undefined= queryResult.data.user.userType;
              if(userType){
                AsyncStorage.setItem(USER_TYPE,userType.toString());
                setUserType(userType);
              }
              hasExist = true;
            }
          }          
        }catch(err){
          logger.error('AccountID could not fetched');
        }
        logger.info('hasExists is',hasExist);
        if(!hasExist){
          const mutationResult = await apolloClient.mutate({mutation:CREATE_USER,variables:{accountId}});
          logger.info('mutationResult',mutationResult);
          userId = mutationResult.data.createUser.id;
        }
        logger.info('FirstScreen userId',userId);
        if(userId){
          AsyncStorage.setItem(USER_ID,userId);
        }
        
        setUserId(userId);
        logger.info('Everything is fine');
        setTimeout(()=>navigation.navigate('ChangeLanguageNav',{isFirstLogin:true}),100);
      }else{
        const userType :string | null = await AsyncStorage.getItem(USER_TYPE);
        if(userType){
           setUserType(parseInt(userType));
        }else{
          setUserType(UserTypeEnum.FREE);
        }
        setUserId(userId);
        setTimeout(()=>navigation.navigate('MainNav',{isFirstLogin:false}),400);    
      }

    }catch(err){
      logger.error('Network error',err);
      Alert.alert(I18n.t('Connection Problem'));
    }
  },[setLang,setUserId,setUserType]);
  return <View style={{flex:1,backgroundColor:'white'}}>
      <ImageBackground source={require('../../assets/Nepanic-Background-01.png')} style={{width:SCREEN_WIDTH,height:SCREEN_HEIGHT-TOP_HEADER_HEIGHT-BOTTOM_TAB_HEIGHT,justifyContent:'center',alignItems:'center'}}>
        <Logo width={SCREEN_WIDTH*0.6} height={SCREEN_WIDTH*0.2}/>
      </ImageBackground>
      
      <CopyRight width={SCREEN_WIDTH} height={TOP_HEADER_HEIGHT}/>
  </View>

}

const initAdmob = async() =>{
  admob()
  .setRequestConfiguration({
    // Update all future requests suitable for parental guidance
    maxAdContentRating: MaxAdContentRating.G,
    // Indicates that you want your content treated as child-directed for purposes of COPPA.
    tagForChildDirectedTreatment: true,
    // Indicates that you want the ad request to be handled in a
    // manner suitable for users under the age of consent.
    tagForUnderAgeOfConsent: true,  })
  .then(() => {
    logger.info('Admob was initialized successfully');
  }).catch((err)=>logger.error('Admob init error is',err));
}

const initNotification = async() =>{
  const authStatus = await messaging().requestPermission();
  logger.info('authStatus is', authStatus);
  //const token = await messaging().getToken();
  //logger.info('TOKEN iss',token);
  
  messaging().setBackgroundMessageHandler(async remoteMessage => {
    logger.info('Message handled in the background!', remoteMessage);
    //Alert.alert('Remote mssage is', JSON.stringify(remoteMessage));
  });
}

const ChangeLanguageScreen = ({ route,navigation }) => {    
    const context = useContext(AppContext);
    const setLang = context.lang.set;  
    const isFirstLogin = (route  && route.params && route.params.isFirstLogin) ? route.params.isFirstLogin : false;
    logger.info('isFirstLogin',isFirstLogin);
    return <View style={{height:SCREEN_HEIGHT,width:SCREEN_WIDTH,backgroundColor:'white'}}>
        <ImageBackground source={require('../../assets/Nepanic-Background-01.png')} style={{height: SCREEN_HEIGHT-TOP_HEADER_HEIGHT-BOTTOM_TAB_HEIGHT,width:SCREEN_WIDTH,justifyContent:'center',alignItems:'center'}}>          
        <TouchableOpacity onPress={()=>{                        
              AsyncStorage.setItem(LANG,'en');
              setLang('en');
              I18n.locale = 'en';
              moment.locale('en');
              if(isFirstLogin){
                navigation.navigate('UserAgreementNav');
              }else{
                navigation.navigate('MainNav');
              }
        }}>
            <ButtonEnglish height={50} width={150}/>        
        </TouchableOpacity>
        <TouchableOpacity style={{marginTop:20}} onPress={()=>{                    
              AsyncStorage.setItem(LANG,'tr');
              setLang('tr');
              I18n.locale = 'tr';
              moment.locale('tr');
              if(isFirstLogin){
                navigation.navigate('UserAgreementNav');
              }else{
                navigation.navigate('MainNav')
              }
        }}>
            <ButtonTurkce height={50} width={150}/>        
        </TouchableOpacity>
        </ImageBackground>
        <CopyRight width={SCREEN_WIDTH} height={TOP_HEADER_HEIGHT}/>
      
      
    </View>
}

const TermsOfUseScreen = ({ navigation }) => {    
  const context = useContext(AppContext);
  const lang = context.lang.get;  
  logger.info('TermsOfUseScreen lang='+lang);
  let uri = '';
  switch(lang){
    case 'tr': uri = 'http://nepanic.com/Appdata/Menu/04-TR-Kullanim-01.png'; break;
    case 'en': uri = 'http://nepanic.com/Appdata/Menu/04-EN-Kullanim-01.png'; break;
    default : uri = 'http://nepanic.com/Appdata/Menu/04-EN-Kullanim-01.png'; break;
  }
  
  return <View>
            <TouchableWithoutFeedback style={{height:50, backgroundColor:'white',justifyContent:'center'}} onPress={()=>navigation.goBack()}>
              <BackButton width={50} height={30} />
            </TouchableWithoutFeedback>
            <ScrollView style={{height:SCREEN_HEIGHT,width:SCREEN_WIDTH,backgroundColor:'white'}}>
              <FetchAndShowImage marginLeft={20} uri={uri} width={SCREEN_WIDTH-40} lang={lang}/>    
            </ScrollView>
         </View>
}

const getTurkishAccept = (navigation) =>{
  return <TouchableOpacity style={{marginLeft:15,marginTop:-10}} onPress={()=>navigation.navigate('TermsOfUseNav')}>
        <Text style={{color:'blue',fontSize:20*fontSizeMultiplier,fontWeight:'500'}}>Kullanıcı Sözleşmesini</Text>
        <Text style={{color:'blue',fontSize:20*fontSizeMultiplier,fontWeight:'500'}}>okudum ve kabul ettim</Text>
      </TouchableOpacity>
}

const getEnglishAccept = (navigation) =>{
  return <TouchableOpacity style={{marginLeft:15,marginTop:-10}} onPress={()=>navigation.navigate('TermsOfUseNav')}>
        <Text style={{color:'blue',fontSize:20*fontSizeMultiplier,fontWeight:'500'}}>I read User Agreement</Text>
        <Text style={{color:'blue',fontSize:20*fontSizeMultiplier,fontWeight:'500'}}>and I accepted it</Text>
      </TouchableOpacity>
}


const UserAgreementScreen = ({ navigation }) => {
  const context = useContext(AppContext);
  const lang = context.lang.get;
  const [toggleCheckBox, setToggleCheckBox] = useState(false);
// style={{marginTop:SCREEN_WIDTH*-0.1}}
  return <ImageBackground source={require('../../assets/Nepanic-Background-01.png')} style={{ alignItems: 'center',justifyContent:'center',width:SCREEN_WIDTH,height:SCREEN_HEIGHT,backgroundColor:'white' }}>    
    
      {lang==='tr' && <WarningTurkish width={SCREEN_WIDTH} height={SCREEN_WIDTH*1.4}/>}
      {lang==='en' && <WarningEnglish width={SCREEN_WIDTH} height={SCREEN_WIDTH*1.4}/>}
      <View style={{flexDirection:'row',marginTop:SCREEN_WIDTH*-0.15}}>
        <CheckBox
        disabled={false}
        value={toggleCheckBox}
        onValueChange={() => toggleCheckBox ? setToggleCheckBox(false) : setToggleCheckBox(true)}
      />
      {lang==='tr' && getTurkishAccept(navigation)}
      {lang==='en' && getEnglishAccept(navigation)}
      </View>
    
    < TouchableOpacity onPress={() => {
      if(toggleCheckBox){
        navigation.navigate('MainNav');
      }else{
        Alert.alert(I18n.t('You have to approve User Aggrement'));
      }
      }}>
      {lang==='tr' && <ButtonTamam width={150} height={50} style={{marginTop:10}}/>}
      {lang==='en' && <ButtonOK width={150} height={50} style={{marginTop:10}}/>}
    </TouchableOpacity>
    <CopyRight width={SCREEN_WIDTH} height={TOP_HEADER_HEIGHT} style={{marginTop:10}}/>
  </ImageBackground>

}
//style={{borderWidth:1,backgroundColor:'dodgerblue',marginTop:30,width:SCREEN_WIDTH/2,height:50,justifyContent:'center',alignItems:'center',borderRadius:25}} 


const styles = StyleSheet.create({
  agreement: {
    fontSize: 16 * fontSizeMultiplier,
  },
});