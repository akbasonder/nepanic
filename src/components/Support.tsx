import React, {  useContext } from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Image,
  Linking,
} from 'react-native';
import { Logger } from '../utils/Logger';
import { fontSizeMultiplier } from '../config/constants';
import { AppContext } from '../../App';
import LogoInstagram from '../../assets/socialmedia/Destek-Icon-1.svg';
import LogoPinterest from '../../assets/socialmedia/Destek-Icon-2.svg';
import LogoTwitter from '../../assets/socialmedia/Destek-Icon-3.svg';
import LogoFacebook from '../../assets/socialmedia/Destek-Icon-4.svg';
import LogoYoutube from '../../assets/socialmedia/Destek-Icon-5.svg';
import Destek1TR from '../../assets/info/destek-tr-1.svg';
import Destek2TR from '../../assets/info/destek-tr-2.svg';
import Destek3TR from '../../assets/info/destek-tr-3.svg';
import Destek4TR from '../../assets/info/destek-tr-4.svg';
import Destek5TR from '../../assets/info/destek-tr-5.svg';
import Destek1EN from '../../assets/info/destek-en-1.svg';
import Destek2EN from '../../assets/info/destek-en-2.svg';
import Destek3EN from '../../assets/info/destek-en-3.svg';
import Destek4EN from '../../assets/info/destek-en-4.svg';
import TitleDestekTR from '../../assets/titles/Title-Tr-Destek.svg';
import TitleDestekEN from '../../assets/titles/Title-En-Destek.svg';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const logger = new Logger('Support');

const iconSize = SCREEN_WIDTH*0.15;

export const SupportScreen = () => {
    const context = useContext(AppContext);
    const lang = context.lang.get;
    //<View style={{marginLeft:20,borderColor:'rgb(224,224,224)',borderWidth:1, width:SCREEN_WIDTH-33}}>
    return <View style={{backgroundColor:'white',height:SCREEN_HEIGHT}}>
        <TopHeader lang={lang}/>
        {lang ==='tr' && getTurkishContent()}
        {lang ==='en' && getEnglishContent()}
        <View style={{justifyContent:'center',flexDirection:'row',width:SCREEN_WIDTH,marginTop:30}}>
            <TouchableOpacity onPress={()=>{Linking.openURL('https://www.instagram.com/nepanicapp/')}} >
                <LogoInstagram width={50} height={50}></LogoInstagram>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{Linking.openURL('https://pinterest.com/nepanicapp')}} style={{marginLeft:20}}>
                <LogoPinterest width={50} height={50}/>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>Linking.openURL('https://twitter.com/nepanicapp')} style={{marginLeft:20}}>
                <LogoTwitter width={50} height={50}/>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{Linking.openURL('https://www.facebook.com/nepanicapp')}} style={{marginLeft:20}}>
                <LogoFacebook width={50} height={50}/>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{Linking.openURL('https://www.youtube.com/channel/UCTXGi7tn16owH8LIt4yu8Ng')}} style={{marginLeft:20}}>
                <LogoYoutube width={50} height={50}/>  
            </TouchableOpacity>
        </View>
    </View>
}

const TopHeader = ({lang='tr'})=>{
    return <View style={{marginLeft:20,marginRight:15, height:70,justifyContent:'center', borderBottomColor:'rgb(224,224,224)',borderBottomWidth:1,borderTopColor:'rgb(224,224,224)',borderTopWidth:1}}>
            {lang==='tr' && <TitleDestekTR width={150} height={50} />}
            {lang==='en' && <TitleDestekEN width={150} height={50} />}            
    </View>
}

const getTurkishContent = () =>{
    return <View style={{alignItems:'center',marginTop:40}}>
        
        <Destek1TR width={SCREEN_WIDTH-80} height={40}/>
        <TouchableOpacity onPress={()=>{
            const url = 'mailto:?cc'
            //(Platform.OS === 'android')     ? 'mailto:?cc=?subject=&body=a'  : 'mailto:?cc=&subject=WaidApp&body=a'  ;
        Linking.openURL(url);
        }}>
            <Destek2TR width={SCREEN_WIDTH/2} height={40}/>
        </TouchableOpacity>
        
        <Destek3TR width={SCREEN_WIDTH-80} height={20}/>

        <Destek4TR width={SCREEN_WIDTH-40} height={60} style={{marginTop:30}}/>

        <Destek5TR width={SCREEN_WIDTH-40} height={40} style={{marginTop:60}}/>
    </View>
}

const getEnglishContent = () =>{
    return <View style={{alignItems:'center',marginTop:40}}>

        <Destek1EN width={SCREEN_WIDTH-80} height={40}/>

        <TouchableOpacity onPress={()=>{
            const url = 'mailto:?cc'
            //(Platform.OS === 'android')     ? 'mailto:?cc=?subject=&body=a'  : 'mailto:?cc=&subject=WaidApp&body=a'  ;
        Linking.openURL(url);
        }}>
            <Destek2EN width={SCREEN_WIDTH/2} height={40}/>
        </TouchableOpacity>
        

        <Destek3EN width={SCREEN_WIDTH-40} height={60} style={{marginTop:30}}/>

        <Destek4EN width={SCREEN_WIDTH-40} height={40} style={{marginTop:60}}/>
    </View>
}


const styles = StyleSheet.create({
    icons: {
        marginLeft:10,
        width:iconSize,
        height:iconSize
    }
   
});