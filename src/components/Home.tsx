import React, { useState, useEffect,Component, useContext } from 'react';
import {
  View,
  ScrollView,
  Text,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Image,
  Alert,  
} from 'react-native';
import { USER_TYPE,UserTypeEnum, fontSizeMultiplier, MENU_TAB_HEADER_INDEX, BOTTOM_TAB_HEIGHT, TOP_HEADER_HEIGHT, BUILD } from '../config/constants';
import I18n from '../config/language';
import { gql, useQuery } from '@apollo/client';
import { showError, showLoading } from './Main'
import { Logger } from '../utils/Logger';
import { createStackNavigator } from '@react-navigation/stack';
import CountDown from '../utils/CountDown' //'react-native-countdown-component';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faVenus, faMars } from '@fortawesome/free-solid-svg-icons';
import { showAd } from '../service/AdMob';
import ImageSize from 'react-native-image-size'
//import { InterstitialAd, RewardedAd, BannerAd, TestIds, BannerAdSize } from '@react-native-firebase/admob';
import MainMenuIcon01TR from '../../assets/home/MainMenu-01.svg';
import MainMenuIcon02TR from '../../assets/home/MainMenu-02.svg';
import MainMenuIcon03TR from '../../assets/home/MainMenu-03.svg';
import MainMenuIcon04TR from '../../assets/home/MainMenu-04.svg';
import MainMenuIcon05TR from '../../assets/home/MainMenu-05.svg';
import MainMenuIcon06TR from '../../assets/home/MainMenu-06.svg';
import MainMenuIcon07TR from '../../assets/home/MainMenu-07.svg';
import MainMenuIcon08TR from '../../assets/home/MainMenu-08.svg';

import MainMenuIcon01EN from '../../assets/home/MainMenu-En-01.svg';
import MainMenuIcon02EN from '../../assets/home/MainMenu-En-02.svg';
import MainMenuIcon03EN from '../../assets/home/MainMenu-En-03.svg';
import MainMenuIcon04EN from '../../assets/home/MainMenu-En-04.svg';
import MainMenuIcon05EN from '../../assets/home/MainMenu-En-05.svg';
import MainMenuIcon06EN from '../../assets/home/MainMenu-En-06.svg';
import MainMenuIcon07EN from '../../assets/home/MainMenu-En-07.svg';
import MainMenuIcon08EN from '../../assets/home/MainMenu-En-08.svg';


import SubMenuTitle01TR from '../../assets/titles/Title-Tr-PanikButonu.svg';
import SubMenuTitle02TR from '../../assets/titles/Title-Tr-Terapiler.svg';
import SubMenuTitle03TR from '../../assets/titles/Title-Tr-DusmaniTani.svg';
import SubMenuTitle04TR from '../../assets/titles/Title-Tr-NefesEgzersizi.svg';
import SubMenuTitle05TR from '../../assets/titles/Title-Tr-Data.svg';
import SubMenuTitle06TR from '../../assets/titles/Title-Tr-Blog.svg';
import SubMenuTitle07TR from '../../assets/titles/Title-Tr-Egzersizler.svg';
import SubMenuTitle08TR from '../../assets/titles/Title-Tr-Beslenme.svg';

import SubMenuTitle01EN from '../../assets/titles/Title-En-PanikButonu.svg';
import SubMenuTitle02EN from '../../assets/titles/Title-En-Terapiler.svg';
import SubMenuTitle03EN from '../../assets/titles/Title-En-DusmaniTani.svg';
import SubMenuTitle04EN from '../../assets/titles/Title-En-NefesEgzersizi.svg';


import SubMenuTitle07EN from '../../assets/titles/Title-En-Egzersizler.svg';
import SubMenuTitle08EN from '../../assets/titles/Title-En-Beslenme.svg';

import SubMenuTitleForMenuTR from '../../assets/titles/Title-Tr-Menu.svg';
import SubMenuTitleForMenuEN from '../../assets/titles/Title-En-Menu.svg';

import BannerSozlerTR from '../../assets/banners/Banner-Sozler-Turkce.svg';
import BannerSozlerEN from '../../assets/banners/Banner-Sozler-English.svg';
import BackButton from '../../assets/buttons/Button-Back-Geri.svg';
import MoveTR from '../../assets/subcategories/TR-Move.svg';
import MoveEN from '../../assets/subcategories/EN-Move.svg';
import { AppContext } from '../../App';
import AsyncStorage from '@react-native-community/async-storage';

import NefesInfoTR from '../../assets/info/Nefes-Yazi-TR.svg';
import NefesInfoEN from '../../assets/info/Nefes-Yazi-EN.svg';
import EgzersizInfoTR from '../../assets/info/Egzersiz-tr.svg';
import EgzersizInfoEN from '../../assets/info/Egzersiz-en.svg';
import ButtonStartTR from '../../assets/buttons/Button-Basla.svg';
import ButtonStartEN from '../../assets/buttons/Button-Start.svg';
import ButtonEgzersizTR from '../../assets/buttons/Button-egzersiz-tr.svg';
import ButtonEgzersizEN from '../../assets/buttons/Button-egzersiz-en.svg';
import { lang } from 'moment';
import FastImage from 'react-native-fast-image'

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const logger = new Logger('Home');


const GET_CATEGORY_INFO_LIST = gql`
  query categoryInfoes($parent:CategoryWhereInput,$languageCode:String!,$version:Int,$groupName:String) {
    categoryInfoes(
      orderBy:rank_ASC
      where:{
      languageCode:$languageCode      
      category:{
        parent:$parent
        status:"active"
        minVersion_lte:$version
        groupName:$groupName
      }
    }  ){
      id
      content
      imageURL
      rank
      category{
        id
        name
        rowHeight
        hasChildren
        columnCount
        navigateTo
      }
    }
  }
`;

const HomeStack = createStackNavigator();

export const HomeScreen = ({ route ,navigation }) => {
  logger.debug('route.params.groupName===',route.params.groupName);
  
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen
        name="Category1Nav"
        component={CategoryScreen}
        initialParams={{"groupName":route.params.groupName}}
        options={{
          headerShown:false,
        }}
      />
      <HomeStack.Screen
        name="Category2Nav"
        component={CategoryScreen}       
      />
      <HomeStack.Screen
        name="Category3Nav"
        component={CategoryScreen}        
      />
      <HomeStack.Screen
        name="Category4Nav"
        component={CategoryScreen}        
      />
      <HomeStack.Screen
        name="BreathNav"
        component={BreathScreen}        
      />
      <HomeStack.Screen
        name="ExercisesNav"
        component={ExercisesScreen}        
      />
    </HomeStack.Navigator>
  )
}

export const CategoryScreen = ({ route, navigation }) => {
  const context = useContext(AppContext);
  
  const parentName = (route && route.params && route.params.parentName) ? route.params.parentName : null;
  const level = (route && route.params && route.params.level) ? route.params.level : 1;
  const parent = parentName?{name:parentName}:null;
  const grpName = (route && route.params && route.params.groupName)?route.params.groupName:undefined;
  const groupName = level>1?undefined: grpName;
  logger.debug('query level=' + level + ' parentName=' + parentName+ ' groupName='+groupName);
  logger.debug('route params',route);
  const languageCode :string = context.lang.get;
  logger.info('languageCode',languageCode);
  const { loading, error, data } = useQuery(GET_CATEGORY_INFO_LIST, { variables: { parent, languageCode, version: BUILD,groupName } });
  logger.log('navigation is', navigation);
  logger.log('route is', route);

  if (loading) return showLoading(languageCode);
  if (error) return showError();
  const userType= context.userType.get;
  const homeBanner = context.homeBanner.get;
  const homeBannerURL = 'http://nepanic.com/Appdata/Banner/0'+homeBanner+'-'+languageCode.toUpperCase()+'-Word-01.png';
  
  logger.info('groupName='+groupName);
  let recordCount = 0;
  const margin = 10;
  let categoryInfoes = [];
  let columnCount = 2;
  let edgeLength = (SCREEN_WIDTH / columnCount) - 15;
  let rowHeight = 40;
  const mainHeight = SCREEN_HEIGHT - BOTTOM_TAB_HEIGHT - TOP_HEADER_HEIGHT;
  const categoryInfoesNumber = data.categoryInfoes.length;
  const iconNumber = groupName==='MenuTab'?MENU_TAB_HEADER_INDEX: level>1 ? route.params.iconNumber:0;

  
  logger.info('data2 is', data);
  const showHeaderBackButton = level!==1 || groupName!=='MenuTab';
  return <View style={{ backgroundColor: 'white',height:mainHeight}}>
    {(level>1 || groupName==='MenuTab') && getHeader(languageCode,groupName==='MenuTab'?MENU_TAB_HEADER_INDEX:iconNumber,navigation,showHeaderBackButton)
    }    
    <ScrollView style={{ width: SCREEN_WIDTH,marginTop:5 }}>
    {data.categoryInfoes.map((categoryInfo, index) => {
      logger.log('categoryInfo index='+index);
      columnCount = categoryInfo.category.columnCount;
      edgeLength = (SCREEN_WIDTH / columnCount) - 15;
      const rh = categoryInfo.category.rowHeight;
      rowHeight = rh === 0 ? mainHeight : rh > 10 ? rh : edgeLength * rh;
      
      logger.info('edgeLength1=' + edgeLength + ' rowHeight1=' + rowHeight + ' columnCount1=' + columnCount);
      recordCount++;
      logger.debug('recordCount='+recordCount);
      if (recordCount % columnCount === 1 ) {
        categoryInfoes = [];
      } 
      if (recordCount % columnCount === 0 || index===categoryInfoesNumber-1 ) {
        if (columnCount === 1) {
          categoryInfoes = [];
        }
        categoryInfoes.push(categoryInfo);
        return <View style={{ flexDirection: 'row'}}>
          {categoryInfoes.map((item,itemIndex) => {
            logger.info('index='+index+' itemIndex='+ itemIndex);
            const navigateTo = item.category.navigateTo?item.category.navigateTo:'Category'+(level+1)+'Nav'
            ////(item.imageURL && !item.content)?
            //TouchableOpacity yerine 
            return item.category.hasChildren ? <TouchableOpacity onPress={() => {
              const iconNumberParam = iconNumber===0? index+itemIndex:iconNumber;
              navigation.navigate(navigateTo, { level: level + 1, parentName: item.category.name,iconNumber:iconNumberParam,groupName });
              logger.info('pressed parentName=' + item.category.name + ' level+1=' + (level + 1));
            }}>              
              {(level===1 && groupName==='HomeTab')?<View style={{marginLeft:margin}}>{getMainMenuIcon(index+itemIndex,languageCode,edgeLength)}</View>
              :item.imageURL ?
              <FetchAndShowImage uri={item.imageURL} marginLeft={margin} width={edgeLength} lang={languageCode}/>
                : <View style={{marginTop:10, width: SCREEN_WIDTH/columnCount-margin-5,marginLeft:margin, height: rowHeight, borderWidth: 1, borderColor: 'rgb(224,224,224)',justifyContent:rowHeight!==mainHeight?'center':undefined,alignItems:'center' }}>
                  <Text style={styles.contentText}>{item.content}</Text>
                </View>
              }
            </TouchableOpacity>
              : <View >
                {userType===UserTypeEnum.FREE && groupName!=='MenuTab' && grpName!=='MenuTab' && showAd(navigation)}
                {item.imageURL  ?
                  <FetchAndShowImage uri={item.imageURL} marginLeft={margin} width={edgeLength} lang={languageCode}/>
                  : <View style={{ width: SCREEN_WIDTH/columnCount, height: rowHeight, borderWidth: 1, borderColor: 'rgb(128,128,128)',justifyContent:rowHeight!==mainHeight?'center':undefined }}>
                    <Text style={styles.contentText}>{item.content}</Text>
                  </View>
                }
              </View>

          })}
        </View>
      }
      categoryInfoes.push(categoryInfo);
    })}

    {level === 1 && groupName==='HomeTab' && <FetchAndShowImage marginLeft={margin} uri={homeBannerURL} lang={languageCode} width={SCREEN_WIDTH - 2 * margin}/>}
    <View style={{height:100}}/>
    
  </ScrollView>
</View>
}

export class FetchAndShowImage extends Component<{uri:string,marginLeft:number,width:number,lang:string},{height:number}> {
  constructor(props:any){
    super(props);
    this.state = {
      height:0
    }
  }

  render() {
    const { width, uri, marginLeft,lang} = this.props;
    const {height} = this.state;    
    return (      
      this.state.height ===0?showLoading(lang): 
      <FastImage style={{ width, height,marginLeft }}
          source={{uri, priority: FastImage.priority.normal,}}
          resizeMode={FastImage.resizeMode.contain}
      />      
    )
  }

  componentDidMount = () =>{
    ImageSize.getSize(this.props.uri).then(size => {
      // size.height
      // size.width
      logger.info('ImageDimension size',size);
      this.setState({height:this.props.width*size.height/size.width});  
    }).catch((err)=>{logger.error('Network error',err); Alert.alert('Connection Problem='+err.message)});
  }
}

const getMainMenuIcon =(index:number,languageCode:string,width:number) =>{  
  logger.info('getMainMenuIcon index='+index+' languageCode='+languageCode);
  const height2 = width*135/192//width*145/192;  
  //return <View style={{borderColor:'black',borderWidth:1,width,height:height2}}/>
  if(languageCode==='tr'){      
      switch(index){
        case 1: return <MainMenuIcon01TR width={width} height={height2}/>
        case 2: return <MainMenuIcon02TR width={width} height={height2}/>
        case 3: return <MainMenuIcon03TR width={width} height={height2}/>
        case 4: return <MainMenuIcon04TR width={width} height={height2}/>
        case 5: return <MainMenuIcon05TR width={width} height={height2}/>
        case 6: return <MainMenuIcon06TR width={width} height={height2}/>
        case 7: return <MainMenuIcon07TR width={width} height={height2}/>
        case 8: return <MainMenuIcon08TR width={width} height={height2}/>
      }
  }
  if(languageCode==='en'){
    switch(index){
      case 1: return <MainMenuIcon01EN width={width} height={height2}/>
      case 2: return <MainMenuIcon02EN width={width} height={height2}/>
      case 3: return <MainMenuIcon03EN width={width} height={height2}/>
      case 4: return <MainMenuIcon04EN width={width} height={height2}/>
      case 5: return <MainMenuIcon05EN width={width} height={height2}/>
      case 6: return <MainMenuIcon06EN width={width} height={height2}/>
      case 7: return <MainMenuIcon07EN width={width} height={height2}/>
      case 8: return <MainMenuIcon08EN width={width} height={height2}/>
    }
}
  return <View/>
}

const getSubMenuIcon =(index:number,languageCode:string) =>{  
  logger.info('getSubMenuIcon index='+index);
  const height:number = 70;
  const width:number = 200;
  if(languageCode==='tr'){
      switch(index){
        case 1: return <SubMenuTitle01TR width={width} height={height}/>
        case 2: return <SubMenuTitle02TR width={width} height={height}/>
        case 3: return <SubMenuTitle03TR width={width} height={height}/>
        case 4: return <SubMenuTitle04TR width={width} height={height}/>
        case 5: return <SubMenuTitle05TR width={width*0.6} height={height}/>
        case 6: return <SubMenuTitle06TR width={width*0.6} height={height}/>
        case 7: return <SubMenuTitle07TR width={width} height={height}/>
        case 8: return <SubMenuTitle08TR width={width} height={height}/>
        case MENU_TAB_HEADER_INDEX: return <SubMenuTitleForMenuTR width={120} height={height-2}/> //For Menu Tab
      }
  }
  if(languageCode==='en'){
    switch(index){
      case 1: return <SubMenuTitle01EN width={width} height={height}/>
      case 2: return <SubMenuTitle02EN width={width} height={height}/>
      case 3: return <SubMenuTitle03EN width={width} height={height}/>
      case 4: return <SubMenuTitle04EN width={width} height={height}/>
      case 5: return <SubMenuTitle05TR width={width*0.6} height={height}/>
      case 6: return <SubMenuTitle06TR width={width*0.6} height={height}/>
      case 7: return <SubMenuTitle07EN width={width} height={height}/>
      case 8: return <SubMenuTitle08EN width={width} height={height}/>
      case MENU_TAB_HEADER_INDEX: return <SubMenuTitleForMenuEN width={120} height={height-2}/> //For Menu Tab
    }
  return <View/>
  }
}

const getHeader = (languageCode:string,iconNumber:number,navigation,showHeaderBackButton=true) =>{
  return <View style={{width:SCREEN_WIDTH }}>
  <View style={{marginLeft:20,borderBottomColor:'rgb(224,224,224)',borderBottomWidth:1,borderTopColor:'rgb(224,224,224)',borderTopWidth:1,flexDirection:'row',justifyContent:'space-between', width:SCREEN_WIDTH-33}}>    
      <View style={{}}>{getSubMenuIcon(iconNumber,languageCode)}</View>
  
  <View style={{alignItems:'flex-end',justifyContent:'center',marginRight:-5}}>
      <TouchableOpacity onPress={()=>{navigation.goBack()}}>
        {showHeaderBackButton && <BackButton width={50} height={30} />}
      </TouchableOpacity>
  </View>
  </View>
</View>
}


const BreathScreen = ({navigation}) => {
  logger.info('BreathScren started');  
  const context = useContext(AppContext);  
  const lang = context.lang.get;  
  const userType = context.userType.get;
  useEffect(() => {
    logger.info('BreathScreen RewardedAd will be userType='+userType);    
    if(!userType || parseInt(userType)===UserTypeEnum.FREE){
      showAd(navigation);
    }      
  },[userType]);

  const [pageState, setPageState] = useState(0);

  return <View style={{backgroundColor:'white',height:SCREEN_HEIGHT-TOP_HEADER_HEIGHT}}>
    {getHeader(lang,4,navigation)}
    {pageState === 0 &&
      <View style={{alignItems:'center'}} >
        {lang==='tr' && <NefesInfoTR width={SCREEN_WIDTH*0.9} height={SCREEN_WIDTH*0.9*730/636} />}
        {lang==='en' && <NefesInfoEN width={SCREEN_WIDTH*0.9} height={SCREEN_WIDTH*0.9*730/636} />}
        <View style={{ width: SCREEN_WIDTH, alignItems: 'center' }}>
          <TouchableOpacity style={{  }}
            onPress={() => setPageState(1)}>
              {lang==='tr'  && <ButtonStartTR width={150} height={50}/>}
              {lang==='en'  && <ButtonStartEN width={150} height={50}/>}
          </TouchableOpacity>
        </View>
      </View>
    }
    {pageState === 1 &&
      <View style={{marginTop:20,justifyContent:'center',alignItems:'center'}}>
        <CountDown
          until={120}
          size={40}
          onFinish={() => setPageState(0)}
          digitStyle={{ backgroundColor: '#FFF' }}
          digitTxtStyle={{ color: 'rgb(160,160,160)' }}
          timeToShow={[ 'S']}          
        />
        {lang==='tr' && <Image source={require('../../assets/subcategories/Breath-TR.gif')} style={{width:SCREEN_WIDTH*0.8,height:SCREEN_WIDTH*0.8*1500/1242}}></Image>}
        {lang==='en' && <Image source={require('../../assets/subcategories/Breath-En.gif')} style={{width:SCREEN_WIDTH*0.8,height:SCREEN_WIDTH*0.8*1500/1242}}></Image>}
        
      </View>
    }

  </View>
}

const ExercisesScreen = ({navigation}) => {
  const context = useContext(AppContext);
  const lang = context.lang.get;
  
  return <View style={{alignItems:'center',width:SCREEN_WIDTH,backgroundColor:'white',height:SCREEN_HEIGHT-TOP_HEADER_HEIGHT}}>
      {getHeader(lang,7,navigation)}
      {lang==='tr' && getExcercisesTurkishContent(navigation)}
      {lang==='en' && getExcercisesEnglishContent(navigation)}
  </View>
}


const getExcercisesTurkishContent = (navigation) => {
  return <View style={{ alignItems: 'center', marginTop: 0 }}>
    <EgzersizInfoTR width={SCREEN_WIDTH*0.9} height={SCREEN_WIDTH*0.8} />
    <TouchableOpacity style={{marginTop:10}}
        onPress={()=>{
          navigation.navigate('Category3Nav',{level:3,parentName:'Egzersizler Alt Menu',iconNumber:7,groupName:'HomeTab'});
        }}>          
          <ButtonEgzersizTR width={SCREEN_WIDTH*0.7} height={70}/>
    </TouchableOpacity>
      
  </View>
}

//TODO: Text yerine Icon olacak
const getExcercisesEnglishContent = (navigation) => {
  return <View style={{ alignItems: 'center', marginTop: 30 }}>
    <EgzersizInfoEN width={SCREEN_WIDTH*0.9} height={SCREEN_WIDTH*0.8} />
    <TouchableOpacity style={{justifyContent:'center',alignItems:'center', marginTop:10,width:SCREEN_WIDTH*0.6,height:70,borderWidth:1,borderColor:'rgb(224,224,224)'}}
        onPress={()=>{
          navigation.navigate('Category3Nav',{level:3,parentName:'Egzersizler Alt Menu',iconNumber:7,groupName:'HomeTab'});
        }}>          
          <Text style={{fontSize:24*fontSizeMultiplier,color:'rgb(128,128,128)',fontWeight:'500'}}>Let's Move</Text>
    </TouchableOpacity>
      
  </View>
}

const styles = StyleSheet.create({
  contentText: {
    //textAlign:'center',
    fontSize: fontSizeMultiplier * 32,
    color:'rgb(128,128,128)',
    
  },
});