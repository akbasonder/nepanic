import React, {useState, useContext, useEffect} from 'react';
import {
  View,
  ImageBackground,
  ScrollView,
  Text,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Image,
  Animated,
  SafeAreaView
} from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faTrophy, } from '@fortawesome/free-solid-svg-icons'
import {HomeScreen} from './Home'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { DiaryScreen } from './Diary';
import { SupportScreen } from './Support';
import { ProScreen } from './Pro';
import {IS_IOS} from '../service/AdMob'
import SubIcon01 from '../../assets/subicons/Icons-01.svg';
import SubIcon02 from '../../assets/subicons/Icons-02.svg';
import SubIcon03 from '../../assets/subicons/Icons-03.svg';
import SubIcon04 from '../../assets/subicons/Icons-04.svg';
import SubIcon05 from '../../assets/subicons/Icons-05.svg';
import SubIcon06 from '../../assets/subicons/Icons-06.svg';
import SubIcon07 from '../../assets/subicons/Icons-07.svg';
import SubIcon08 from '../../assets/subicons/Icons-08.svg';
import SubIcon09 from '../../assets/subicons/Icons-09.svg';
import SubIcon10 from '../../assets/subicons/Icons-10.svg';
import HeaderV2 from '../../assets/headers/Header-V2.svg';
import Logo from '../../assets/headers/Logo.svg';
import LoadingTR from '../../assets/Loading-tr.svg';
import LoadingEN from '../../assets/Loading-en.svg';
import SloganTR from '../../assets/headers/Slogan-Tr.svg';
import SloganEN from '../../assets/headers/Slogan-En.svg';
import HeaderTR from '../../assets/headers/Header-TR.svg';
import HeaderEN from '../../assets/headers/Header-EN.svg';
import { TOP_HEADER_HEIGHT, BOTTOM_TAB_HEIGHT } from '../config/constants';
import { Logger } from '../utils/Logger';
import { AppContext } from '../../App';
import I18n from '../config/language';
import WarningHataTR from '../../assets/warnings/Uyari-Hata.svg';
import WarningHataEN from '../../assets/warnings/Uyari-Hata-En.svg';
import { BottomTabBarProps } from "@react-navigation/bottom-tabs";
import { useSafeArea } from "react-native-safe-area-context";



const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

const MainTab = createBottomTabNavigator();
const logger = new Logger('Main');

export const showLoading = (lang:string) =>{    
    //<ImageBackground source={require('../../assets/Loading.gif')} style={{width:SCREEN_WIDTH,height:SCREEN_HEIGHT-50,justifyContent:'center',alignItems:'center'}}>
    //{showIcon && <Logo width={SCREEN_WIDTH*0.6} height={SCREEN_WIDTH*0.2}/>}
    return <View style={{width:SCREEN_WIDTH,height:SCREEN_HEIGHT-TOP_HEADER_HEIGHT-BOTTOM_TAB_HEIGHT}}>
      {lang==='tr' && <LoadingTR width={SCREEN_WIDTH} height={SCREEN_HEIGHT-TOP_HEADER_HEIGHT-BOTTOM_TAB_HEIGHT}/>}
      {lang==='en' && <LoadingEN width={SCREEN_WIDTH} height={SCREEN_HEIGHT-TOP_HEADER_HEIGHT-BOTTOM_TAB_HEIGHT}/>}
    </View>
}

export const showError = () =>{
  const context = useContext(AppContext);
  const lang = context.lang.get;
  return <View style={{width:SCREEN_WIDTH,height:SCREEN_HEIGHT-TOP_HEADER_HEIGHT-BOTTOM_TAB_HEIGHT,alignItems:'center'}}>
      {lang === 'tr' && <WarningHataTR width={SCREEN_WIDTH} height={SCREEN_WIDTH*1.6} />}
      {lang === 'en' && <WarningHataEN width={SCREEN_WIDTH} height={SCREEN_WIDTH*1.6} />}
        
  </View>
}

export const MainHeader = ({navigation}) =>{
  //logger.debug('MainHeader navigation is',navigation);
  const height1 = (TOP_HEADER_HEIGHT-10)*0.65;
  const height2 = (TOP_HEADER_HEIGHT-10)*0.35;
  const context = useContext(AppContext);
  const lang = context.lang.get;
  logger.info('SCREEN_WIDTH='+SCREEN_WIDTH+' SCREEN_HEIGHT='+SCREEN_HEIGHT)
  const insets = useSafeArea();
  const spaceTop = insets.top?insets.top-15:0;//0;//(IS_IOS && SCREEN_HEIGHT/SCREEN_WIDTH===896/414)?30:0;
  //const spaceTop = -10//(IS_IOS && SCREEN_HEIGHT/SCREEN_WIDTH===896/414)?30:0;
  return <View style={{width:SCREEN_WIDTH,height:TOP_HEADER_HEIGHT+spaceTop,backgroundColor:'white'}}>
      <View style={{height:spaceTop+10}}/>
    <View style={{width:SCREEN_WIDTH,height:TOP_HEADER_HEIGHT-10,flexDirection:'row'}}>
      <View style={{flex:0.2,justifyContent:'center',marginLeft:20}}>
        <HeaderV2 width={height1} height={height1}/>
      </View>
      <View style={{flex:0.6,justifyContent:'center',alignItems:'center',flexDirection:'column'}}>
        <Logo width={height1*6} height={height1}/>
        {lang==='tr' && <SloganTR width={height2*5} height={height2}></SloganTR>}
        {lang==='en' && <SloganEN width={height2*5} height={height2}></SloganEN>}
      </View>
      <TouchableOpacity style={{flex:0.2,justifyContent:'center',alignItems:'flex-end',marginRight:20}}
          onPress={()=>navigation.navigate('ChangeLanguageNav',{isFirstLogin:false})}>
        {lang==='tr' && <HeaderTR width={height1} height={height1}/>}
        {lang==='en' && <HeaderEN width={height1} height={height1}/>}
      </TouchableOpacity>
    </View>
    
  </View>
}


export const MainTabs = () =>{
  
  return (
    <View style={{ flex: 1, position: "relative"}}>
    <MainTab.Navigator
      initialRouteName="HomeNav"
      tabBar={(props: BottomTabBarProps) => <TabBar {...props} />}      
    >
      <MainTab.Screen
        name="HomeNav"
        component={HomeScreen}
        initialParams={{groupName:'HomeTab'}}      
      />
      <MainTab.Screen
        name="DiaryNav"
        component={DiaryScreen}                
      />
      <MainTab.Screen
        name="SupportNav"
        component={SupportScreen}               
      />
      <MainTab.Screen
        name="ProNav"
        component={ProScreen}                
      />
      <MainTab.Screen
        name="MenuNav"
        component={HomeScreen}        
        initialParams={{groupName:'MenuTab'}}
      />
    
    </MainTab.Navigator>
    {//TODO: check without useSafeArea
    useSafeArea().bottom > 0 && (
        <View
          style={{
            height: useSafeArea().bottom - 5,
            backgroundColor: "white",
          }}
        />
      )}
    </View>
  );
}




const TabBar = ({
  state,
  descriptors,
  navigation,
}: BottomTabBarProps) => {
  const [translateValue] = useState(new Animated.Value(0));
  const totalWidth = Dimensions.get("window").width;
  const tabWidth = totalWidth / state.routes.length;
  const context = useContext(AppContext);
  const homeBanner = context.homeBanner.get;
  const setHomeBanner = context.homeBanner.set;

  return (
    <View style={[style.tabContainer, { width: totalWidth }]}>
   
      <View style={{ flexDirection: "row" }}>
        <Animated.View
          style={[
            style.slider,
            {
              transform: [{ translateX: translateValue }],
              width: tabWidth - 20,
            },
          ]}
        />

        {state.routes.map((route, index) => {
          const { options } = descriptors[route.key];
          const label =
            options.tabBarLabel !== undefined
              ? options.tabBarLabel
              : options.title !== undefined
              ? options.title
              : route.name;

          const isFocused = state.index === index;

          const onPress = () => {
            if(index===0){
              if(homeBanner===5){
                setHomeBanner(1);
              }else{
                setHomeBanner(homeBanner+1);
              }             
            }

            const event = navigation.emit({
              type: "tabPress",
              target: route.key,
              canPreventDefault: true,
            });

            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route.name);
            }

            Animated.spring(translateValue, {
              toValue: index * tabWidth,
              velocity: 10,
              useNativeDriver: true,
            }).start();
          };

          const onLongPress = () => {
            navigation.emit({
              type: "tabLongPress",
              target: route.key,
            });
          };

          return (
            <TouchableOpacity
              accessibilityRole="button"
              //accessibilityStates={isFocused ? ["selected"] : []}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              onPress={onPress}
              onLongPress={onLongPress}
              style={{ flex: 1 }}
              key={index}
            >
              <BottomMenuItem                
                index={index}
                isCurrent={isFocused}
              />
            </TouchableOpacity>
          );
        })}
      </View>
    </View>
  );
};

type BottomMenuItemProps = {
  index:number
  isCurrent?: boolean;
};

const iconEdge = 30;
const TabIcons  =[
  <SubIcon01 style={{width:iconEdge,height:iconEdge,marginTop:10}}/>,
  <SubIcon02 style={{width:iconEdge,height:iconEdge,marginTop:10}}/>,
  <SubIcon03 style={{width:iconEdge,height:iconEdge,marginTop:10}}/>,
  <SubIcon04 style={{width:iconEdge,height:iconEdge,marginTop:10}}/>,
  <SubIcon05 style={{width:iconEdge,height:iconEdge,marginTop:10}}/>,
  <SubIcon06 style={{width:iconEdge,height:iconEdge,marginTop:10}}/>,
  <SubIcon07 style={{width:iconEdge,height:iconEdge,marginTop:10}}/>,
  <SubIcon08 style={{width:iconEdge,height:iconEdge,marginTop:10}}/>,
  <SubIcon09 style={{width:iconEdge,height:iconEdge,marginTop:10}}/>,
  <SubIcon10 style={{width:iconEdge,height:iconEdge,marginTop:10}}/>,
]

const TabMenuNameKeys = [
  'Home',
  'My Diary',
  'Support',
  'Pro',
  'Menu'
]

const BottomMenuItem = ({ index, isCurrent }: BottomMenuItemProps) => {
  return (
    <View
      style={{
        height: "100%",        
        alignItems: "center",
      }}
    >
      {TabIcons[index*2+(isCurrent?1:0)]}
    <Text style={{color:isCurrent?'rgb(0,49,255)':'gray'}}>{I18n.t(TabMenuNameKeys[index])}</Text>
    </View>
  );
};

const style = StyleSheet.create({
  tabContainer: {
    height: BOTTOM_TAB_HEIGHT,
    shadowOffset: {
      width: 0,
      height: -1,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4.0,
    backgroundColor: "white",
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    elevation: 10,
    position: "absolute",
    bottom: 0,
  },
  slider: {
    height: 5,
    position: "absolute",
    top: 0,
    left: 10,
    backgroundColor: 'blue',
    borderRadius: 10,
  },
});