/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import * as React from 'react';
import {useEffect,useState} from 'react'
import {View,StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {AuthScreen} from './src/components/Auth';
import {MainTabs, MainHeader} from './src/components/Main';
import { ApolloProvider } from '@apollo/client';
import { apolloClient } from './src/service/GraphQLClient';
import { UserTypeEnum } from './src/config/constants';

const Stack = createStackNavigator();
export const AppContext = React.createContext({});

export default function App() {
  StatusBar.setHidden(true);
  const [userId, setUserId] = useState('');
  const [lang, setLang] = useState('tr');
  const [userType,setUserType] = useState(UserTypeEnum.FREE);
  const rand = Math.random();
  const index = Math.floor((rand*5)+1);
  const [homeBanner,setHomeBanner] = useState(index>=5?5:index);
  
  const store = {
    userId: { get: userId, set: setUserId },
    lang: { get: lang, set: setLang },
    userType:{get:userType,set:setUserType},
    homeBanner:{get:homeBanner,set:setHomeBanner},
  };
  
  return (
    <ApolloProvider client={apolloClient}>    
     <AppContext.Provider value={store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName={'AuthNav'}>          
          <Stack.Screen name="MainNav" component={MainTabs} options={{
            gestureEnabled: false,
            header:props=><MainHeader navigation={props.navigation}/>      
          }}/>
          <Stack.Screen name="AuthNav" component={AuthScreen} options={{ headerShown: false, gestureEnabled: false }}/>                    
        </Stack.Navigator>
      </NavigationContainer>     
      </AppContext.Provider>
      </ApolloProvider>    
  );
}
